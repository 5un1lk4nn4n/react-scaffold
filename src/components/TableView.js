import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';

import {
  Anchor,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow,
  Text,
  Menu,
  Button,
  TextInput,
} from 'grommet';
import { Filter, Configure, LinkPrevious, Add } from 'grommet-icons';

import { COLORS } from '../styles';

const TableWrapper = styled.div`
  table {
    border-radius: 10px;
    width: 100%;
  }

  tr {
    border-left: 3px solid transparent;
  }
  thead {
    background-color: orange;
    border-radius: 10px;
    color: ${COLORS.quartenary};
    font-weight: bold;
    tr {
      background-color: orange;
    }
  }

  tbody {
    tr:nth-child(even) {
      background-color: ${COLORS.backgrounder};
      border-radius: 20px;
    }
    tr {
      :hover {
        border-left: 3px solid ${COLORS.primary};
        border-radius: 20px;
      }
    }
  }
`;

const Badge = styled.label`
  background: ${props => props.color};
  padding: 3px 10px;
  border-radius: 15px;
  color: white;
  font-size: 12px;
  font-weight: bold;
`;

const data = [
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 1,
    created_at: '23.4.5',
    type: 'premium',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'premium',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: 0,
    created_at: '23.4.5',
    type: 'user',
    email: 'sunil@gmail.com',
  },
];

const displayRecord = (rowData, type) => {
  // By default some key will be shown in special format, rest in normal text
  switch (type) {
    case 'is_active':
      return <Badge color={rowData ? 'green' : 'red'}>{rowData ? 'active' : 'inactive'}</Badge>;
    case 'created_at':
      return <Text weight="bold">{rowData}</Text>;
    case 'type':
      return (
        <Text weight="bold" color={rowData === 'premium' ? COLORS.primary : COLORS.secondary}>
          {rowData}
        </Text>
      );
    default:
      return <Text>{rowData}</Text>;
  }
};
const Row = ({ row }) => {
  return (
    <TableRow>
      {Object.keys(row).map(c => (
        <TableCell key={row[c]} scope="col">
          {displayRecord(row[c], c)}
        </TableCell>
      ))}
      {<ActionCell />}
    </TableRow>
  );
};

const Header = ({ row }) => {
  row.push('options');
  return (
    <TableRow
      style={{
        backgroundColor: 'background',
        padding: '5px',
        borderWidth: 0,
        borderRadius: '15px',
      }}
    >
      {row.map(c => (
        <TableCell
          key={c.property}
          scope="col"
          align={c === 'options' ? 'end' : 'start'}
          style={{
            borderWidth: 0,
          }}
        >
          <Text>{_.startCase(c)}</Text>
        </TableCell>
      ))}
    </TableRow>
  );
};

const ActionCell = () => (
  <TableCell align="end">
    <Menu
      dropProps={{ align: { top: 'bottom', left: 'left' } }}
      size="small"
      label="..."
      icon={false}
      items={[
        { label: 'Full Info', onClick: () => {} },
        { label: 'Send Email', onClick: () => {} },
        { label: 'Active', onClick: () => {} },
        { label: 'InActive', onClick: () => {} },
        { label: 'Disabled', disabled: true },
      ]}
    />
  </TableCell>
);

const TableView = () => {
  return (
    <Box background="white">
      <Box
        justify="between"
        align="center"
        direction="row"
        width="100%"
        alignSelf="center"
        background="white"
      >
        <Box>
          <Anchor
            icon={<LinkPrevious size="small" />}
            label="Back to Home"
            href="/"
            size="xsmall"
          />
        </Box>
        <Box
          width="40%"
          direction="row"
          align="center"
          gap="small"
          background={COLORS.backgrounder}
          style={{
            borderRadius: '20px',
          }}
          margin="medium"
          pad={{
            horizontal: 'small',
          }}
        >
          <TextInput icon={<Filter />} placeholder="Filter User ..." size="small" plain />
          <Button icon={<Configure />} onClick={() => {}} size="small" />
        </Box>
        <Button
          icon={<Add size="small" color="white" />}
          label="ADD"
          onClick={() => {}}
          primary
          size="medium"
        />
      </Box>
      <TableWrapper>
        <Table>
          <TableHeader>{<Header row={Object.keys(data[0])} />}</TableHeader>
          <TableBody>
            {data.map((datum, key) => (
              <Row row={datum} key={`table-${key}`} />
            ))}
          </TableBody>
        </Table>
      </TableWrapper>
    </Box>
  );
};

export default TableView;
