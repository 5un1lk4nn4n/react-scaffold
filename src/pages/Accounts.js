import React, { Component, createRef } from 'react';
import { Box, Image, Text, Tab, Tabs } from 'grommet';
import { withRouter } from 'react-router-dom';

import { MENU } from '../constants';
import styled from 'styled-components';
import Settings from '../pages/Settings';
import Help from '../pages/Help';
const TAB_COMPONENTS = {
  'my-preferences': () => <div>Preferences yet to come</div>,
  'payment-and-invoices': () => <div>Invoice yet to come</div>,
  'change-password': Settings,
  'help-and-support': Help,
};

const TabWrapper = styled.div`
  border-bottom-color: 'black';
  border-bottom-width: 1px;
  border-radius: 5px;
`;

class Accounts extends Component {
  constructor(props) {
    super(props);
    this.searchBoxRef = createRef();
    this.state = {
      loading: true,
      index: 0,
    };
  }

  onActive = nextIndex => {
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {}

  render() {
    const { index } = this.state;
    return (
      <Box
        background="white"
        pad={0}
        width={`${window.innerWidth}px`}
        height="100vh"
        margin={{
          top: '50px',
        }}
      >
        <TabWrapper>
          <Tabs activeIndex={index} onActive={this.onActive}>
            {MENU.topBar.map((item, key) => {
              const TabComponent = TAB_COMPONENTS[item.component];
              return (
                <Tab title={item.label} key={`tab-bar-${key}`}>
                  <Box margin="small" pad="small" background="white">
                    {<TabComponent />}
                  </Box>
                </Tab>
              );
            })}
          </Tabs>
        </TabWrapper>
      </Box>
    );
  }
}

export default withRouter(Accounts);
